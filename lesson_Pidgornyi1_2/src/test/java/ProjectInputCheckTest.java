import static org.junit.Assert.*;

public class ProjectInputCheckTest {

    @org.junit.Test
    public void myProjectCheckInIf() {
        ProjectInputCheck projectInputCheck = new ProjectInputCheck();
        projectInputCheck.myProjectCheckInIf("1");
        projectInputCheck.myProjectCheckInIf("1");
        projectInputCheck.myProjectCheckInIf("2");
        projectInputCheck.myProjectCheckInIf("0");
    }

    @org.junit.Test
    public void myProjectCheckInSwitch() {
        ProjectInputCheck projectInputCheck = new ProjectInputCheck();
        projectInputCheck.myProjectCheckInSwitch("4");
        projectInputCheck.myProjectCheckInSwitch("5");
        projectInputCheck.myProjectCheckInSwitch("6");
        projectInputCheck.myProjectCheckInSwitch("33");
    }
}