import java.util.Arrays;

public class MyArray {


        public static void myArraysBubble() {
            int[] array = {8, 2, 4, 3, 7, 0, 1, 14, 48, 87, 12};
            System.out.println("Вы ввели массив чисел: " + Arrays.toString(array));
            boolean flag = false;
            int tmp, i = 0, max = 0, min = 0;

            while (!flag) {
                flag = true;
                //проходим все элементы
                for (i = 1; i < array.length; i++) {
                    //обратный проход который не сравнивает последний элемент
                    for (int j = array.length - 1; j >= i; j--) {
                        //сортировка
                        if (array[j] < array[j - 1]) {
                            flag = false;
                            tmp = array[j - 1];
                            array[j - 1] = array[j];
                            array[j] = tmp;
                        }
                    }
                }
            }
            System.out.println("Результат сортировки по возрастанию: " + Arrays.toString(array));

            while (flag) {
                flag = false;
                for (i = 1; i < array.length; i++) {
                    for (int j = array.length - 1; j >= i; j--) {
                        if (array[j] > array[j - 1]) {
                            tmp = array[j - 1];
                            array[j - 1] = array[j];
                            array[j] = tmp;
                            flag = true;
                        }
                    }
                }
            }
            System.out.println("Результат сортировки по убыванию: " + Arrays.toString(array));
            for (i = 0; i < array.length; i++){
                if (array[i] > max){
                max = array[i];
                }
                if (array[i] < min){
                min = array[i];
                }
            }
            System.out.println("максимальный элемент: "+max);
            System.out.println("минимальный элемент: "+min);
        }


        public static void mySearchElementArray(){
            int [][] array = {
                    {3, 7, 4, 0, 8, 1, 2, 5},
                    {11, 12, 6, 7, 10, 17}
            };
            int max = 0, min = 0;
            System.out.println("Поиск элемента массива: ");
            for (int i = 0; i <array.length; i++){
                for (int j = 0; j < array[i].length; j++){
                    if (array [i][j] > 0 && array[i][j] > max){
                        if (array[i][j] > max) {
                            max = array[i][j];
                        }
                    }
                    System.out.print("  "+array[i][j]+"  ");
                }
                System.out.println("\n"+"Наибольший элемент массива: " + max);
            }
            System.out.println("Наибольший элемент двух массивов: " + max);
        }

        public static void main(String[] args) {
            myArraysBubble();
            mySearchElementArray();
        }

    }