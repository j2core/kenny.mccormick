public class MyArrayExample {

    //сортировка пузырьком

    public static void myBubbleSort(){
        int [] array = { 5, 7, 9, 1, 3, 2 };


        bubbleSortMax(array);
        System.out.println("сортировка по возрастанию: ");
        for (int i = 0; i < array.length; i++){
            System.out.print( array[i]+ " ");
        }


        bubbleSortMin(array);
        System.out.println("\n"+"сортировка по убыванию: ");

        for (int i = 0; i < array.length; i++){
            System.out.print( array[i]+ " ");
        }

    }

    public static void bubbleSortMax (int [] array){
        int tmp = 0;

        for (int i = 1; i < array.length; i++){
            for (int j = array.length-1; j>=i; j--){
                if (array[j-1] > array[j]){
                    tmp = array[j-1];
                    array[j-1] = array[j];
                    array[j] = tmp;
                }
            }
        }
    }

    public static void bubbleSortMin (int [] array){
        int tmp = 0;

        for (int i = 1; i < array.length; i++){
            for (int j = array.length-1; j>=i; j--){
                if (array[j-1] < array[j]){
                    tmp = array[j];
                    array[j] = array[j-1];
                    array[j-1] = tmp;
                }
            }
        }
    }

    //поиск наибольшего и наименьшего элемента в двумерном массиве int

    public static void myFindElementArray (){
        int [][] array = {
                {10, 11, 23, 56, 74, 12, -14, 0},
                {101, 13, 93, -17, 17, -5}
        };
        int max = 0, min = 0, searchNumber = 12, x = 0, y = 0, x1 = 0, y1 = 0, z1 = 0, z2 = 0;


        System.out.println("\nПоиск элемента массива: ");

        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array[i].length; j++){
                if (array[i][j] > max) {
                    max = array[i][j];
                    x1 = i;
                    y1 = j;
                    }
                if (array[i][j] < min){
                    min = array[i][j];
                    x = i;
                    y = j;
                }
                if (array[i][j] == searchNumber) {
                    z1 = i;
                    z2 = j;
                }
                System.out.print("  "+array[i][j]+"  ");
            }
            System.out.println("\n");
        }
        System.out.println(" Наибольший элемент двух массивов: " + max+" индекс: ["+x1+"]["+y1+"]");
        System.out.println(" Наименьший элемент двух массивов: " + min+" индекс: ["+x+"]["+y+"]");
        System.out.println("поиск элемента: "+searchNumber+"\n Успешно найдено: " +searchNumber+" индекс ["+z1+"]["+z2+"]");
    }



    public static void main(String[] args) {
        myBubbleSort();
        myFindElementArray();
    }
}
