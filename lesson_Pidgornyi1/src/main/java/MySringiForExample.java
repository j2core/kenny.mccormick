import java.lang.Character;

public class MySringiForExample {
    public static void main(String[] args) {
        myString();
        doubleMyExample();
        myDouble();
        myIntAndInteger();
        myCharAndCharacter();
    }
// static что бы не зморачиваться и иметь доступ к методам;

    static void myString() {
        String str1= "Jela Murzikowna";
        String str2= "Jela ";
        String str3= "Murzikowna";
        String str4= str2 + str3;
        System.out.println(str1 == (str2 + str3) ? true : false);           //false
        System.out.println(str1.equals(str2 + str3) ? true : false);        //true
        System.out.println(str4 == str1 ? true : false);                    //false
        System.out.println(str1.equals(str4) ? true : false);               //true
    }

    static void doubleMyExample() {
        double d1 = 0.3;
        double d2 = 0.4;
        double d3 = d1 + d2;
        System.out.println((d1 + d2) == d3 ? true : false);
    }

    static void myDouble() {
        Double dd1 = 0.7;
        Double dd2 = 0.7;
        Double dd3 = dd1 + dd2;
        System.out.println(dd1 == dd2 ? true : false);
        System.out.println(dd1.equals(dd2) ? true : false);
        System.out.println(dd3.equals(dd1 + dd2) ? true : false);
    }

    static void myIntAndInteger(){
        int a = 1, b = 1;
        System.out.println(a == b ? true : false);
        Integer c = 1, d = 1;
        System.out.println(c == d ? true : false);
        System.out.println(c.equals(d) ? true : false);
    }

    static void myCharAndCharacter(){
        char charA = '1', charB = '1';
        System.out.println(charA == charB ? true : false);
        Character character1 = 'f', character2 = 'f';
        System.out.println(character1 == character2 ? true : false);
        System.out.println(character1.equals(character2) ? true : false);

    }
}
